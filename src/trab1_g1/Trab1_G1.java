/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trab1_g1;

import java.util.Random;

/**
 *
 * @author MiguelS-PC
 */
public class Trab1_G1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int visitantes[];
        int total=0,dias=27, dia;
        double media;
        Random gerar= new Random();
        visitantes= new int[dias];
        
        System.out.println("Insira a quantidade de visitantes");
        for (int i=0; i<dias; i++)
        {
            dia=i+1;
            System.out.println("Dia "+ dia);
            /*Está a gerar automaticamente para não ter de inserir todos os valores à mão*/
            visitantes[i]=gerar.nextInt(10000);            
        }
        
        for (int i=0; i<dias; i++)
        {
           total += visitantes[i];
        }
         media=total/dias;
         System.out.println("Média diária:" + media);
        
    }
    
}
